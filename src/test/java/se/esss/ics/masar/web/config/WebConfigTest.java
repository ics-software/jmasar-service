/**
 * Copyright (C) 2018 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package se.esss.ics.masar.web.config;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.ContextHierarchy;
import org.springframework.test.context.junit4.SpringRunner;

import se.esss.ics.masar.epics.config.EpicsConfiguration;
import se.esss.ics.masar.persistence.config.PersistenceConfiguration;
import se.esss.ics.masar.services.IServices;
import se.esss.ics.masar.services.config.ServicesConfiguration;

@RunWith(SpringRunner.class)
@ContextHierarchy({ @ContextConfiguration(classes = {
		EpicsConfiguration.class,
		PersistenceConfiguration.class, 
		ServicesConfiguration.class, 
		WebConfiguration.class})})
public class WebConfigTest {
	
	@Autowired
	private IServices services;
	
	@BeforeClass
	public static void init() {
		System.setProperty("dbengine", "h2");
	}

	@Test
	public void testWbeConfig() {
		Assert.assertNotNull(services);
	}
}
