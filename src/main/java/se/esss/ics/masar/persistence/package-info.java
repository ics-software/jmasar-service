/**
 * This package contains the database access API, defined in {@link se.esss.ics.masar.persistence.dao.NodeDAO}.
 * 
 * <p>
 * See {@link se.esss.ics.masar.persistence.config.PersistenceConfiguration} for information on how to configure the data source. The implementation attempts to be
 * database engine agnostic, but does not make use of any ORM framework.
 * </p>
 * 
 */

package se.esss.ics.masar.persistence;